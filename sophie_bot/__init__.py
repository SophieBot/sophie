from aiogram import Bot, Dispatcher
from aiogram.client.default import DefaultBotProperties
from aiogram.client.session.aiohttp import AiohttpSession
from aiogram.client.telegram import PRODUCTION, TelegramAPIServer
from aiogram.fsm.storage.base import DefaultKeyBuilder
from aiogram.fsm.storage.memory import SimpleEventIsolation
from aiogram.fsm.storage.redis import RedisStorage
from redis.asyncio import Redis

from sophie_bot.config import CONFIG
from sophie_bot.utils.logger import log
from sophie_bot.versions import SOPHIE_BRANCH, SOPHIE_COMMIT, SOPHIE_VERSION

log.info("Sophie!")
log.info("Version: " + SOPHIE_VERSION)
log.info("Commit: " + SOPHIE_COMMIT)
log.info("Branch: " + SOPHIE_BRANCH)

# Support for custom BotAPI servers
bot_api = TelegramAPIServer.from_base(str(CONFIG.botapi_server)) if CONFIG.botapi_server else PRODUCTION
session = AiohttpSession(api=bot_api)
log.info(f"Using BotAPI server: {bot_api}")

# AIOGram
bot = Bot(token=CONFIG.token, default=DefaultBotProperties(parse_mode="html"), session=session)
redis = Redis(
    host=CONFIG.redis_host,
    port=CONFIG.redis_port,
    db=CONFIG.redis_db_states,
    single_connection_client=True,
)
storage = RedisStorage(redis=redis, key_builder=DefaultKeyBuilder(prefix=str(CONFIG.redis_db_fsm)))
dp = Dispatcher(storage=storage, events_isolation=SimpleEventIsolation())
