from aiogram.utils.i18n import ConstI18nMiddleware
from ass_tg.middleware import ArgsMiddleware

from sophie_bot import CONFIG, dp
from sophie_bot.middlewares.beta import BetaMiddleware
from sophie_bot.middlewares.connections import ConnectionsMiddleware
from sophie_bot.middlewares.disabling import DisablingMiddleware
from sophie_bot.middlewares.legacy_save_chats import LegacySaveChats
from sophie_bot.middlewares.localization import LocalizationMiddleware
from sophie_bot.middlewares.logic import OrMiddleware
from sophie_bot.middlewares.memory_debug import TracemallocMiddleware
from sophie_bot.middlewares.save_chats import SaveChatsMiddleware
from sophie_bot.services.i18n import i18n
from sophie_bot.utils.logger import log

localization_middleware = LocalizationMiddleware(i18n)
try_localization_middleware = OrMiddleware(localization_middleware, ConstI18nMiddleware("en_US", i18n))


def enable_middlewares():
    if CONFIG.debug_mode:
        from .debug import UpdateDebugMiddleware

        dp.update.middleware(UpdateDebugMiddleware())

    dp.update.middleware(localization_middleware)

    if CONFIG.proxy_enable:
        log.info("Enabled Proxy!")
        dp.update.middleware(BetaMiddleware())

    dp.message.middleware(ArgsMiddleware(i18n=i18n))

    dp.update.outer_middleware(SaveChatsMiddleware())
    dp.message.outer_middleware(LegacySaveChats())

    dp.update.middleware(ConnectionsMiddleware())
    dp.message.middleware(DisablingMiddleware())

    if CONFIG.debug_mode:
        from .debug import DataDebugMiddleware, HandlerDebugMiddleware

        dp.update.middleware(DataDebugMiddleware())
        dp.update.middleware(HandlerDebugMiddleware())

    if CONFIG.memory_debug:
        dp.update.middleware(TracemallocMiddleware())
