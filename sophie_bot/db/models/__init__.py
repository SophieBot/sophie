from typing import List, Type

from beanie import Document

from sophie_bot.db.models.ai_autotranslate import AIAutotranslateModel
from sophie_bot.db.models.ai_enabled import AIEnabledModel
from sophie_bot.db.models.ai_moderator import AIModeratorModel
from sophie_bot.db.models.ai_usage import AIUsageModel
from sophie_bot.db.models.beta import BetaModeModel
from sophie_bot.db.models.chat import ChatModel, ChatTopicModel, UserInGroupModel
from sophie_bot.db.models.chat_connections import ChatConnectionModel
from sophie_bot.db.models.disabling import DisablingModel
from sophie_bot.db.models.filters import FiltersModel
from sophie_bot.db.models.greetings import GreetingsModel
from sophie_bot.db.models.language import LanguageModel
from sophie_bot.db.models.notes import NoteModel
from sophie_bot.db.models.privatenotes import PrivateNotesModel
from sophie_bot.db.models.rules import RulesModel
from sophie_bot.db.models.settings_keyvalue import GlobalSettings
from sophie_bot.db.models.ws_user import WSUserModel

models: List[Type[Document]] = [
    ChatModel,
    UserInGroupModel,
    ChatTopicModel,
    LanguageModel,
    ChatConnectionModel,
    NoteModel,
    BetaModeModel,
    GlobalSettings,
    AIEnabledModel,
    AIUsageModel,
    AIAutotranslateModel,
    AIModeratorModel,
    DisablingModel,
    PrivateNotesModel,
    RulesModel,
    GreetingsModel,
    WSUserModel,
    FiltersModel,
]
