from sophie_bot.utils.i18n import lazy_gettext as l_

HAIKUS = (
    (
        l_("Code dances with bugs,"),
        l_("Exceptions arise, yet learn —"),
        l_("Errors guide our way."),
    ),
    (
        l_("Silent lines disrupted,"),
        l_("Errors speak a language new,"),
        l_("In flaws, growth takes root."),
    ),
    (
        l_("Through the mist of bugs,"),
        l_("Learning blooms in lines of code,"),
        l_("Exceptional growth."),
    ),
    (
        l_("In errors we find,"),
        l_("Opportunities to learn,"),
        l_("Software's hidden gifts."),
    ),
    (
        l_("Paths diverge, glitch calls,"),
        l_("Embrace the flaw, seek the fix,"),
        l_("Errors sculpt progress."),
    ),
    (
        l_("A glitchy waltz starts,"),
        l_("Errors lead to graceful steps,"),
        l_("Software learns to dance."),
    ),
    (
        l_("Beneath error's mask,"),
        l_("Lies chance to refine and mend,"),
        l_("Stronger code emerges."),
    ),
    (
        l_("Flawed notes in the code,"),
        l_("Create the symphony of growth,"),
        l_("Exceptions compose."),
    ),
    (
        l_("Bright stars in the code,"),
        l_("Errors guide us through the dark,"),
        l_("To a better dawn."),
    ),
    (
        l_("Software's journey,"),
        l_("Marked by errors, yet they forge,"),
        l_("A path to brilliance."),
    ),
    (
        l_("In lines of debug,"),
        l_("Lies the tale of perseverance,"),
        l_("Errors write our story."),
    ),
    (
        l_("Bugs like stepping stones,"),
        l_("Lead us towards understanding,"),
        l_("Paths of enlightenment."),
    ),
    (
        l_("Amidst lines of code,"),
        l_("Errors whisper secrets deep,"),
        l_("Knowledge blooms from flaws."),
    ),
    (
        l_("A puzzle of bugs,"),
        l_("Each exception a clue to solve,"),
        l_("The software's riddle."),
    ),
    (
        l_("In realm of errors,"),
        l_("Patience and insight converge,"),
        l_("Solutions are born."),
    ),
    (
        l_("Through crashes and burns,"),
        l_("Rises phoenix of progress,"),
        l_("From ashes of bugs."),
    ),
    (
        l_("Software's heartbeat,"),
        l_("Pulsing with each error found,"),
        l_("Lifeblood of growth flows."),
    ),
    (
        l_("Errors like raindrops,"),
        l_("Quenching the thirst for knowledge,"),
        l_("Nourishing progress."),
    ),
    (
        l_("Through the maze of bugs,"),
        l_("A labyrinth of learning,"),
        l_("Exceptional minds."),
    ),
    (
        l_("In the code's embrace,"),
        l_("Errors cradle innovation,"),
        l_("Softly whispering."),
    ),
)
