from sophie_bot.modules.restrictions.actions.ban import BanModernAction
from sophie_bot.modules.restrictions.actions.kick import KickModernAction
from sophie_bot.modules.restrictions.actions.mute import MuteModernAction

__modern_actions__ = (KickModernAction, BanModernAction, MuteModernAction)
