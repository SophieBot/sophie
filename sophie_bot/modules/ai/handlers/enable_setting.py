from aiogram import flags
from aiogram.dispatcher.event.handler import CallbackType

from sophie_bot.db.models.ai_enabled import AIEnabledModel
from sophie_bot.filters.admin_rights import UserRestricting
from sophie_bot.filters.cmd import CMDFilter
from sophie_bot.modules.utils_.status_handler import StatusBoolHandlerABC
from sophie_bot.utils.i18n import lazy_gettext as l_


@flags.help(description=l_("Controls AI features"))
class EnableAI(StatusBoolHandlerABC):
    header_text = l_("✨ AI Features")
    change_command = "aienable"

    @staticmethod
    def filters() -> tuple[CallbackType, ...]:
        return CMDFilter(("enableai", "aienable")), UserRestricting(admin=True)

    async def get_status(self) -> bool:
        chat_id = self.connection.id
        db_model = await AIEnabledModel.get_state(chat_id)
        return bool(db_model)

    async def set_status(self, new_status: bool):
        chat_id = self.connection.id
        await AIEnabledModel.set_state(chat_id, new_status)
