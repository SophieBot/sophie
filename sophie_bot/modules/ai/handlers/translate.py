from aiogram import flags
from aiogram.dispatcher.event.handler import CallbackType
from aiogram.handlers import MessageHandler
from aiogram.types import Message
from ass_tg.types import TextArg
from stfu_tg import Bold, Doc, HList, PreformattedHTML, Section, Template, Title

from sophie_bot import bot
from sophie_bot.filters.cmd import CMDFilter
from sophie_bot.modules.ai.filters.ai_enabled import AIEnabledFilter
from sophie_bot.modules.ai.fsm.pm import AI_GENERATED_TEXT
from sophie_bot.modules.ai.json_schemas.translate import AITranslateResponseSchema
from sophie_bot.modules.ai.utils.ai_chatbot import ai_generate_schema
from sophie_bot.modules.ai.utils.message_history import AIMessageHistory
from sophie_bot.modules.ai.utils.transform_audio import transform_voice_to_text
from sophie_bot.modules.notes.utils.unparse_legacy import legacy_markdown_to_html
from sophie_bot.utils.i18n import gettext as _
from sophie_bot.utils.i18n import lazy_gettext as l_
from sophie_bot.utils.logger import log


async def text_or_reply(message: Message | None, _data: dict):
    if message and message.reply_to_message:
        return {}
    return {
        "text": TextArg(l_("Text to translate")),
    }


@flags.help(
    alias_to_modules=["language"],
    description=l_(
        "Translates the given (or replied) text to the chat's selected language. Also transcribes the "
        "replied voice message to text"
    ),
)
@flags.disableable(name="translate")
@flags.ai_cache(cache_handler_result=True)
class AiTranslate(MessageHandler):
    @staticmethod
    def filters() -> tuple[CallbackType, ...]:
        return CMDFilter(("aitranslate", "translate", "tr")), AIEnabledFilter()

    async def handle(self):
        is_autotranslate: bool = self.data.get("autotranslate", False)
        await bot.send_chat_action(self.event.chat.id, "typing")

        language_name = self.data["i18n"].current_locale_display

        is_voice = False
        if self.event.reply_to_message and self.event.reply_to_message.voice and not is_autotranslate:
            to_translate = await transform_voice_to_text(self.event.reply_to_message.voice)
            is_voice = True
        elif self.event.reply_to_message and not is_autotranslate:
            to_translate = self.event.reply_to_message.text or ""
        elif self.data.get("voice"):
            to_translate = ""
            is_voice = True
        else:
            to_translate = self.data.get("text", "")

        # AI Context
        ai_context = AIMessageHistory()
        await ai_context.add_from_message_with_reply(self.event)
        ai_context.add_system(
            "\n".join(
                (
                    _("You're the professional AI translator/transcriber."),
                    _(f"Translate the following text to {language_name}:\n{to_translate}"),
                )
            )
        )

        translated = await ai_generate_schema(ai_context, AITranslateResponseSchema)

        # Prevent extra translating
        if is_autotranslate and not is_voice and not translated.needs_translation:
            log.debug("AiTranslate: AI do not think it needs translation, skipping.")
            return
        elif to_translate.lower().strip() == translated.translated_text.lower().strip():
            log.debug("AiTranslate: AI gave the exact same text, skipping.")
            return

        doc = Doc(
            HList(
                Title(AI_GENERATED_TEXT),
                _("Auto Translator") if is_autotranslate else _("Translator"),
                f"({_('Voice')})" if is_voice else None,
            ),
            (
                Bold(
                    Template(
                        _("From {from_lang} to {to_lang}"),
                        from_lang=f"{translated.origin_language_emoji} {translated.origin_language_name}",
                        to_lang=language_name,
                    )
                )
                if not is_voice
                else None
            ),
            PreformattedHTML(legacy_markdown_to_html(translated.translated_text)),
            (
                Section(translated.translation_explanations, title=_("Translation Notes"))
                if translated.translation_explanations
                else None
            ),
        )

        await self.event.reply(str(doc))
