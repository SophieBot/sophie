from sophie_bot.utils.i18n import lazy_gettext as l_

AI_POLICY = l_("By using AI features you agree to the our Privacy Policy (/privacy) and third party AI services used.")
