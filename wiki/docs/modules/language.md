
# Language 🌍


## Available commands


### Only admins

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/lang` | - | Asks for the new language for the chat. |  |

### Aliased commands from [✨ Sophie AI](ai)

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/aiautotranslate` `/autotranslate` | `<?New status>` | Controls AI Auto translator |  |
| `/aitranslate` `/translate` `/tr` | `<Text to translate>` | Translates the given (or replied) text to the chat's selected language. Also transcribes the replied voice message to text | *Disable-able* |