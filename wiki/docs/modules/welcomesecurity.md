
# Welcome Security 🛡️

> Welcome Security contains a bunch of tools that can help filter bots that tries to join your groups, as well as make sure the new users acknowledged the chat rules before being able to speak

## Available commands


### Commands

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/welcomesecurity` | - | Shows Welcome Security settings | *Disable-able* |

### Only admins

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/enablewelcomecaptcha` | `<?New status>` | Shows / changes the state of Welcome Captcha. |  |
| `/welcomerestrict` | `<?New status or restrict time>` | Shows / changes the state of Welcome Restrict (Media restricting). |  |