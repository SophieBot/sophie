
# Reports 🗳


## Available commands


### Commands

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/report` | - | Reports the replied message. |  |