
# Warnings ⚠️


## Available commands


### Commands

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/warns` | `<User>` | Shows the warnings history. |  |

### Only admins

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/warn` | `<User>` | Warns the user. |  |
| `/warnlimit` | `<New limit>` | Sets the limit of warnings in the chat. |  |
| `/resetwarns` `/delwarns` | `<User>` | Deletes all warnings |  |
| `/warnmode` `/warnaction` | `<New mode>` | Changes the punishment action after hitting the warning limit. |  |