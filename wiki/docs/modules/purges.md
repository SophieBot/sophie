
# Purges 🗑


## Available commands


### Only admins

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/delete` `/del` | - | Deletes the replied message | *Only in groups* |
| `/purge` | - | Purges all messages after replied message (including the replied message) | *Only in groups* |
---
> ⚠️ Telegram does not allow bots to delete messages older than 48h. Deleting / purging messages older than this will not work. Please do this manually.