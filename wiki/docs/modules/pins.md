
# Pins 📌


## Available commands


### Only admins

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/unpin` | - | Pins replied message |  |
| `/pin` | - | Unpins the last pinned message |  |