
# Help ℹ️


### Provides helpful information


## Available commands


### Commands

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/help` | - | Shows the help message | *Only in groups*, *Disable-able* |
| `/start` | - | Shows the start message | *Only in groups*, *Disable-able* |

### PM-only

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/help` | - | Shows help overview for all modules | *Only in groups* |