
# Federations 🏙


## Available commands


### Commands

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/newfed` `/fnew` | `<Command>` | Creates a new Federation. |  |
| `/joinfed` `/fjoin` | `<Fed ID>` | Joins the current chat to the federation. |  |
| `/leavefed` `/fleave` | - | Leaves the current chat from the federation. |  |
| `/fsub` | `<Fed ID to subscribe>` | Subscribes federation to another. |  |
| `/funsub` | `<Subscribed Fed ID>` | Unsubscribes the federation. |  |
| `/fpromote` | `<User>` | Promotes the user to the Federation admins. |  |
| `/fdemote` | `<User>` | Demotes the user from the Federation admins. |  |
| `/fsetlog` `/setfedlog` | - | Sets the Federation logs channel. | *Only in groups* |
| `/funsetlog` `/unsetfedlog` | - | Removes the Federation logs channel. | *Only in groups* |
| `/fchatlist` `/fchats` | - | Shows a list of chats in the Federation. |  |
| `/fadminlist` `/fadmins` | - | Shows a list of admins in the Federation. |  |
| `/finfo` `/fedinfo` | - | Shows the information about the Federation. |  |
| `/fban` `/sfban` | `<User>` | Bans the user in the whole Federation. |  |
| `/unfban` `/funban` | `<User>` | Unbans the user from the Federation. |  |
| `/delfed` `/fdel` | - | Deletes the federation. |  |
| `/frename` | `<New name>` | Renames the federation. |  |
| `/fbanlist` `/exportfbans` `/fexport` | - | Exports the list of bans. |  |
| `/importfbans` `/fimport` | - | Imports the list of bans. |  |
| `/fcheck` `/fbanstat` | - | Checks the user ban status (can be used to check yourself). |  |