
# Privacy 🕵️‍♂️️


### Data protection


## Available commands


### PM-only

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/privacy` | - | Shows the privacy policy of the bot | *Only in groups* |
| `/export` | - | Exports your data to a JSON file | *Only in groups* |