
# Connections 🔐


## Available commands


### Commands

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/connect` | - | Connects PM with bot to the current chat. | *Only in groups* |

### PM-only

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/connect` | - | Connects to the latest chat. |  |
| `/connect` | `<Chat ID>` | Connected to the chat by its ID. |  |
| `/disconnect` | - | Disconnects the PM from connected chat. |  |

### Only admins

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/allowusersconnect` | `<New state>` | Sets whatever normal users (non admins) are allowed to connect |  |