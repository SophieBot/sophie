
# Rules 🪧


## Available commands


### Commands

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/rules` | - | Gets chat rules | *Disable-able* |

### Only admins

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/setrules` | `<Content>` | Sets chat rules | *Only in groups* |
| `/resetrules` | - | Resets chat rules to default settings. | *Only in groups* |