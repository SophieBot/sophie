
# Disabling 🚫


## Available commands


### Commands

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/disableable` | - | Lists all commands that can be disabled. | *Disable-able* |
| `/disabled` | - | Lists all disabled commands. | *Disable-able* |

### Only admins

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/disable` | `<Command>` | Disables the command. |  |
| `/enable` | `<Command>` | Enables previously disabled command. |  |
| `/enableall` | - | - |  |