
# Promotes ⭐️


## Available commands


### Only admins

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/promote` | `<User>` `<?Admin title>` | Promotes the user to admins. |  |
| `/demote` | `<User>` | Demotes the user from admins. |  |