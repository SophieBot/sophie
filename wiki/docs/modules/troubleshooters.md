
# Troubleshooters 🧰

> Small commands for fixing problems and issues

## Available commands


### Commands

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/cancel` | - | Cancel current state, use if Sophie is not responding on your message |  |

### Only admins

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/enablebeta` | `<Preferred strategy mode>` | Set preferred strategy mode |  |
| `/enablebeta` | - | Get current strategy mode / current state |  |
| `/admincache` | - | Reset admin rights cache, use if Sophie didn't get the recently added admin |  |