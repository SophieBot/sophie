
# Sophie AI ✨


### Rainbow sparkles and shininess

> Sophie supports quite a few ways to use AI features. \
> From a simple chat-bot, to the automatic translator. Have fun. \
>   \
> By using AI features you agree to the our Privacy Policy (/privacy) and third party AI services used. \
> Please note that you can make a limited amount of AI requests per day.

## Available commands


### Commands

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/aitranslate` `/translate` `/tr` | `<Text to translate>` | Translates the given (or replied) text to the chat's selected language. Also transcribes the replied voice message to text | *Disable-able* |
| `/ai` | `<Prompt>` | Ask Sophie a question | *Disable-able* |

### PM-only

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/ai` | - | Start the AI ChatBot mode | *Only in groups* |

### Only admins

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/enableai` `/aienable` | `<?New status>` | Controls AI features |  |
| `/aimoderator` | `<?New status>` | Controls AI Moderator features |  |
| `/aiautotranslate` `/autotranslate` | `<?New status>` | Controls AI Auto translator |  |
| `/aisave` | `<Prompt>` | Generate a new note using AI |  |
| `/aireset` | - | Reset the chat's AI context |  |