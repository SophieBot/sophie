
# Users 🫂


## Available commands


### Commands

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/info` | - | Shows the additional information about the user. | *Disable-able* |
| `/id` `/chatid` `/userid` | - | Shows IDs. | *Disable-able* |
| `/adminlist` `/admins` | - | Lists all the chats admins. | *Disable-able* |