
# Antiflood 📈


## Available commands


### Only admins

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/setflood` | `<Number of messages>` | Set the antiflood limit for this chat. |  |
| `/antiflood` `/flood` | `<New state>` | Controls the antiflood state. |  |
| `/setfloodaction` | `<New action>` | Sets the antiflood action. |  |