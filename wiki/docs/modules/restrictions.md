
# Restrictions 🛑


## Available commands


### Only admins

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/kick` `/skick` | `<User>` | Kicks the user from the chat. The user would be able to join back. |  |
| `/mute` `/smute` `/tmute` `/stmute` | `<User>` | Mutes the user. |  |
| `/unmute` | `<User>` | Unmutes the user (also lets the user send media). |  |
| `/ban` `/sban` `/tban` `/stban` | `<User>` | Unmutes the user (also lets the user send media). |  |
| `/unban` | `<User>` | Unbans the user. |  |

### Aliased commands from [✨ Sophie AI](ai)

| Commands | Arguments | Description | Remarks |
| --- | --- | --- | --- |
| `/aimoderator` | `<?New status>` | Controls AI Moderator features |  |